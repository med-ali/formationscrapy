# -*- coding: utf-8 -*-
# encoding=utf8 
import pdb
import re
#pdb.set_trace() 
import encodings
import sys  
#reload(sys) 
import json 
import codecs
#sys.setdefaultencoding('utf-8')
import scrapy
from scrapy.spider import BaseSpider
from scrapy.selector import HtmlXPathSelector
from scrapy import Selector
from formation.items import FormationItem
import requests
import logging
from scrapy import log
class MySpider(scrapy.Spider):
    name = 'pagespider'
    def start_requests(self):
        Min = "0"
        Max = "1000000000000"
        start_urls = "http://www.residences-immobilier.com/fr/recherche.html?lang=FR&setLocDep=&departement=&district=&villes=&TypeAnnonceV=VEN&ville_dep=&enlarge_search=&TypeBien=&bdgMin="+Min+"&bdgMax="+Max+"&nb_piece=&surfMin=&surfMax=&keywords=&page="
        for i in range(1,1660):
            urlpage = start_urls + str(i)
            yield scrapy.Request(url=urlpage,callback=self.parse_one)


    def parse_one(self,response):
        taburl = response.xpath("//a[@class='more_details show-for-medium js-annonce-link']/@href").extract()
        for url in taburl:
            yield scrapy.Request(url=url,callback=self.parse_details)
            #yield {"url":url}


    def parse_details(self,response):
        item = FormationItem()
        retaille = r'(\d)'
        urls = []
        item["urlannonces"] = response.url
        item["titre"] = response.xpath("//span[@class='ta_tb']/text()").extract_first().replace('-',"")
        if response.xpath("//span[@itemprop='price']/text()").extract_first():
           item["prix"] = response.xpath("//span[@itemprop='price']/text()").extract_first().replace("Prix : ","")
        else:
            item["prix"] = "plus de 10ME"
        item["ville"] = response.xpath("//span[@class='ville']/text()").extract_first()
        """if re.match(retaille, response.xpath("//span[@itemprop='floorSize']/text()").extract_first()):
            item["taille"] = response.xpath("//span[@itemprop='floorSize']/text()").extract_first() + "m²"
        else:
            item["taille"] = """
        """div = response.xpath("//div[@class='desc_ann']").extract_first()
        divs = Selector(text=div)"""
        #parg = divs.xpath("//p[@class='hlight']/text()").extract()[1]
        expression = r"^{+}\d"
        #div = response.xpath("//div[@class='desc_ann']").extract_first()
        form_popup = response.xpath("//form[@id='askRappel_form']").extract_first()
        form_popups = Selector(text=form_popup)
        item["IdAnnonce"] = form_popups.xpath("//input[@id='IdAnnonce']/@value").extract_first()
        item["IdAgence"] = form_popups.xpath("//input[@id='IdAgence']/@value").extract_first()
        item["Reference"] = form_popups.xpath("//input[@id='Reference']/@value").extract_first()
        try:
            div = response.xpath("//div[@class='columns medium-12 agency hlight']").extract_first()
            divagence = Selector(text=div)
            item["NomAgence"] = divagence.xpath("//p[@class='legalName']/text()").extract_first()
            item["AddresseAgence"] = divagence.xpath("//p[@class='address']/text()").extract_first()
            item["CodePostal"] = divagence.xpath("//p[@itemprop='address']/text()").extract()[1].split()[0]
        except:
            pass
       
        urlagence = "http://www.residences-immobilier.com/fr/ajax/agence-tel-"+str(item["IdAgence"])+"-"+str(item["IdAnnonce"])+".html"
        #url_tel  = "http://www.residences-immobilier.com/fr/ajax/annonce-tel-"+str(item["IdAnnonce"])+".html"
        yield scrapy.Request(url=urlagence,meta={"item":item},callback=self.parse_get_tel_agence)
        #yield item
        


    def parse_get_tel_agence(self,response):
        #pdb.set_trace()
        item = response.meta["item"]
        res = response.text.replace('[',"") 
        telagence = res.replace(']',"")
        telagence = telagence.replace('\"',"")
        item["TelAgence"] = telagence
        yield item